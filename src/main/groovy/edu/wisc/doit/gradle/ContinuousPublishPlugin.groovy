package edu.wisc.doit.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * Root Gradle {@link Plugin} class.
 *
 * Referenced in META-INF/gradle-plugins/edu.wisc.doit.gradle.continuous-publish-plugin.properties.
 *
 * @author Nicholas Blair
 */
class ContinuousPublishPlugin implements Plugin<Project> {
  @Override
  void apply(Project project) {
    project.getExtensions()
      .create("cpublish",
        ContinuousPublishPluginExtension.class)
    project.getTasks()
      .create("confirmProjectVersionIncremented",
        ConfirmProjectVersionIncrementedTask.class)
  }
}
