package edu.wisc.doit.gradle

import com.github.zafarkhaja.semver.Version
import org.gradle.api.GradleException

/**
 * Encapsulates logic for comparing version strings.
 *
 * @author Nicholas Blair
 */
class CompareVersions {

  /**
   *
   * @param fromGitDescribe the version string from git (e.g. 'git describe')
   * @param inBuildGradle the version found in the project version property
   * @return none
   * @throws org.gradle.api.GradleException if version inBuildGradle is equal to or less than fromGitDescribe
   */
  static def compare(String fromGitDescribe, String inBuildGradle) {
    if(!fromGitDescribe?.trim()) {
      println "git describe is empty - there are no releases yet. You must have at least one tag on the repository to use this task."
      return
    }
    def lastVersion = Version.valueOf(fromGitDescribe)
    if(!''.equals(lastVersion.getPreReleaseVersion())) {
      // git describe produced something like '0.20.7-1-gfbe61fd'
      // split on '-' and recreate last as the first token
      lastVersion = Version.valueOf(fromGitDescribe.split('-')[0])
    }

    // read 'version' field from build.gradle
    def currentVersion = Version.valueOf(inBuildGradle);
    if ( currentVersion.greaterThan(lastVersion) ) {
      // success, the project version is greater than 'last.released.version'
      println "Version test successful: proposed version is " + currentVersion + " greater than last.released.version " + lastVersion
    } else {
      throw new GradleException("version field in build.gradle with current value " + currentVersion + " must be incremented past " + lastVersion)
    }
  }
}
