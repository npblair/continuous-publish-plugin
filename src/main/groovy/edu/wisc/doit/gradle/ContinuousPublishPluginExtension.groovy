package edu.wisc.doit.gradle

/**
 * Bean containing settings for this plugin.
 *
 * @author Nicholas Blair
 */
class ContinuousPublishPluginExtension {
  boolean skip = false
}
