package edu.wisc.doit.gradle

import org.gradle.api.GradleException
import org.junit.Test

/**
 * Unit tests for {@link CompareVersions}.
 *
 * @author Nicholas Blair
 */
class CompareVersionsTest {

  @Test
  public void no_tags_yet() {
    CompareVersions.compare(null, "0.0.1-SNAPSHOT")
  }
  @Test
  public void whitespace() {
    CompareVersions.compare('   ', "0.0.1-SNAPSHOT")
  }
  @Test
  public void tag_exists_and_version_incremented() {
    CompareVersions.compare("0.0.1", "0.0.2-SNAPSHOT")
  }
  @Test(expected = GradleException.class)
  public void tag_exists_and_version_not_incremented_expects_failure() {
    CompareVersions.compare("0.0.1", "0.0.1")
  }
  @Test(expected = GradleException.class)
  public void describe_includes_hash_same_version_expects_failure() {
    CompareVersions.compare("0.20.7-1-gfbe61fd", "0.20.7")
  }
}
